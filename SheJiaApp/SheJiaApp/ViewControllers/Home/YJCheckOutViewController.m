//
//  YJCheckOutViewController.m
//  SheJiaApp
//
//  Created by 阳剑 on 16/6/28.
//  Copyright © 2016年 阳剑. All rights reserved.
//

#import "YJCheckOutViewController.h"
#import "YJCheckOutTableViewCell.h"


@interface YJCheckOutViewController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) YJCheckOut *checkOut;

@end


@implementation YJCheckOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.tableView registerNib:[UINib  nibWithNibName:@"YJCheckOutTableViewCell" bundle:nil] forCellReuseIdentifier:@"CheckOutCell"];
    [self requestData];
    [self initNavigationBar];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    //设置table的属性
    self.tableView.estimatedRowHeight=300;
    self.tableView.backgroundColor =NavColor;
    self.tableView.rowHeight=UITableViewAutomaticDimension;
    self.tableView.separatorStyle =UITableViewCellSeparatorStyleNone;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 30;
//}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  self.checkOut.validates.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *validateArray = self.checkOut.validates;
    NSError *err = nil;
    YJValidate *validate = [[YJValidate alloc] initWithDictionary:validateArray[indexPath.row] error:&err];
    if(err)
    {
        NSLog(@"%@",err);
    }
    YJCheckOutTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CheckOutCell"];
    if(!cell)
    {
        cell = [[YJCheckOutTableViewCell alloc] init];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.validate = validate;
    return cell;
}
#pragma mark -Private Methods
- (void)requestData
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:CHECKOUT_URL parameters:@{@"validateid" : DEFAULT} progress:^(NSProgress * _Nonnull uploadProgress) {
        NSLog(@"正在加载鉴定浏览页面");
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *err = nil;
        NSLog(@"加载鉴定界面完成");
         _checkOut = [[YJCheckOut alloc] initWithDictionary:responseObject error:&err];
        if(err)
        {
            NSLog(@"%@",err);
        }
        [self.tableView reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error);
    }];
}

- (void)initNavigationBar
{
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"navigation-back"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    backItem.tintColor = [UIColor grayColor];
    self.navigationItem.leftBarButtonItem = backItem;
    
    
    
}
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
