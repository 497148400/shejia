//
//  Model.h
//  SheJiaApp
//
//  Created by 阳剑 on 16/6/28.
//  Copyright © 2016年 阳剑. All rights reserved.
//

#ifndef Model_h
#define Model_h

#import "YJHomePage.h"
#import "YJBanner.h"
#import "YJSector.h"
#import "YJTag.h"
#import "YJProduct.h"
#import "YJCheckOut.h"
#import "YJValidate.h"
#import "YJUser.h"

#endif /* Model_h */
